import csv
import os
import unittest
from unittest.mock import patch, Mock

from extraction_data_to_csv import process_xml_file
from preparation_zip_files import create_xml_file_and_get_filename


class BaseTestCase(unittest.TestCase):

    RANDOM_NUMBER = 5
    UNIQUE_STRING = 'UNIQUE_STRING'
    RANDOM_STRING = 'random_string'

    sample_filename = './{}.xml'.format(UNIQUE_STRING)
    # Объектов тут должно быть строго RANDOM_NUMBER
    sample_xml_file_template = "<root>"\
                             '<var name="id" value="{unique_str}" />'\
                             '<var name="level" value="{number}" />'\
                             '<objects>'\
                             '<object name="{random_str}" />'\
                             '<object name="{random_str}" />'\
                             '<object name="{random_str}" />'\
                             '<object name="{random_str}" />'\
                             '<object name="{random_str}" />'\
                             '</objects>'\
                             '</root>'

    def get_sample_xml_file_content(self):
        return self.sample_xml_file_template.format(
            number=self.RANDOM_NUMBER,
            unique_str=self.UNIQUE_STRING,
            random_str=self.RANDOM_STRING
        )


class TestXmlFilePreparation(BaseTestCase):

    def test_valid_xml_file(self):
        filename = create_xml_file_and_get_filename(dirname='.')
        self.assertEqual(filename, self.sample_filename)

        with open(filename) as xml_file:
            self.assertEqual(self.get_sample_xml_file_content(), xml_file.read())

    def setUp(self):
        randrange_patcher = patch('random.randrange', new=Mock(return_value=self.RANDOM_NUMBER))
        uuid_patcher = patch('uuid.uuid4', new=Mock(return_value=self.UNIQUE_STRING))
        random_string_patcher = patch('preparation_zip_files.get_random_str', new=Mock(return_value=self.RANDOM_STRING))
        self.patchers = [randrange_patcher, uuid_patcher, random_string_patcher]
        [p.start() for p in self.patchers]

    def tearDown(self):
        [p.stop() for p in self.patchers]
        os.remove(self.sample_filename)


class TestCsvFilesGeneration(BaseTestCase):

    LEVELS_CSV_FILENAME = 'levels_info_file.csv'
    OBJECTS_CSV_FILENAME = 'objects_info_file.csv'

    def test_valid_csv_files(self):
        with open(self.sample_filename, 'w') as xml_file:
            xml_file.write(self.get_sample_xml_file_content())

        process_xml_file(
            self.sample_filename,
            self.levels_info_file_writer,
            self.objects_info_file_writer
        )
        self.levels_info_file.close()
        self.objects_info_file.close()

        with open(self.LEVELS_CSV_FILENAME) as levels_file:
            self.assertEqual(
                '{},{}\n'.format(self.UNIQUE_STRING, self.RANDOM_NUMBER),
                levels_file.read()
            )
        with open(self.OBJECTS_CSV_FILENAME) as objects_file:
            self.assertEqual(
                '{},{}\n'.format(self.UNIQUE_STRING, self.RANDOM_STRING) * self.RANDOM_NUMBER,
                objects_file.read()
            )

    def setUp(self):
        self.levels_info_file = open('levels_info_file.csv', 'w', newline='')
        self.objects_info_file = open('objects_info_file.csv', 'w', newline='')
        self.levels_info_file_writer = csv.writer(self.levels_info_file)
        self.objects_info_file_writer = csv.writer(self.objects_info_file)

    def tearDown(self):
        os.remove(self.LEVELS_CSV_FILENAME)
        os.remove(self.OBJECTS_CSV_FILENAME)
