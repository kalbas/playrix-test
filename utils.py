import os
import string
import random


def get_random_str(length=8):
    return ''.join(random.choice(string.ascii_lowercase) for _ in range(length))


def create_empty_dir(dirname):
    try:
        os.mkdir(dirname)
    except FileExistsError:
        remove_files_in_dir(dirname)


def remove_files_in_dir(dirname):
    with os.scandir(dirname) as directory:
        for element in directory:
            os.remove(element.path)
