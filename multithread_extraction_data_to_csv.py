import csv
import os
from multiprocessing import Pool
from zipfile import ZipFile
from xml.etree import ElementTree as ET


def walk_through_zip_files(dirname):
    levels_info_file = open('levels_info_file.csv', 'w', newline='')
    objects_info_file = open('objects_info_file.csv', 'w', newline='')
    levels_info_file_writer = csv.writer(levels_info_file)
    objects_info_file_writer = csv.writer(objects_info_file)

    with os.scandir(dirname) as directory:
        for element in directory:
            if element.is_file and element.name.split('.')[-1] == 'zip':
                process_zip_file(element.path, levels_info_file_writer, objects_info_file_writer)

    levels_info_file.close()
    objects_info_file.close()


def process_zip_file(filename, levels_file, objects_file):
    zip_file = ZipFile(filename)

    xml_strings = []
    for filename in zip_file.namelist():
        with zip_file.open(filename) as xml_file:
            xml_strings.append(xml_file.read())

    pool = Pool(4)
    results = pool.map(parse_xml_file, xml_strings)
    pool.close()
    pool.join()

    for result in results:
        write_data_to_csv(result, levels_file, objects_file)


def parse_xml_file(xml_string):
    root = ET.fromstring(xml_string)

    file_id = root.findall("./*[@name='id']")[0].attrib['value']
    level_id = root.findall("./*[@name='level']")[0].attrib['value']
    elements = [element.attrib['name'] for element in root.findall(".//objects/*")]

    return file_id, level_id, elements


def write_data_to_csv(data, levels_file, objects_file):
    file_id, level_id, objects_name = data

    levels_file.writerow([file_id, level_id])
    for object_name in objects_name:
        objects_file.writerow([file_id, object_name])
