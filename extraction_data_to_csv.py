import csv
import os
from zipfile import ZipFile
from xml.etree import ElementTree as ET


def walk_through_zip_files(dirname):
    levels_info_file = open('levels_info_file.csv', 'w', newline='')
    objects_info_file = open('objects_info_file.csv', 'w', newline='')
    levels_info_file_writer = csv.writer(levels_info_file)
    objects_info_file_writer = csv.writer(objects_info_file)

    with os.scandir(dirname) as directory:
        for element in directory:
            if element.is_file and element.name.split('.')[-1] == 'zip':
                process_zip_file(element.path, levels_info_file_writer, objects_info_file_writer)

    levels_info_file.close()
    objects_info_file.close()


def process_zip_file(filename, levels_file, objects_file):
    zip_file = ZipFile(filename)
    for filename in zip_file.namelist():
        if filename.split('.')[-1] == 'xml':
            xml_file = zip_file.open(filename)
            process_xml_file(xml_file, levels_file, objects_file)


def process_xml_file(xml_file, levels_file, objects_file):
    tree = ET.parse(xml_file)
    root = tree.getroot()

    file_id = root.findall("./*[@name='id']")[0].attrib['value']
    level_id = root.findall("./*[@name='level']")[0].attrib['value']

    elements = [element for element in root.findall(".//objects/*")]

    for element in elements:
        objects_file.writerow([file_id, element.attrib['name']])
    levels_file.writerow([file_id, level_id])
