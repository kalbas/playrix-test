import random
import uuid
from xml.etree import ElementTree as ET
from zipfile import ZipFile

from utils import get_random_str, create_empty_dir, remove_files_in_dir


MIN_OBJECTS_COUNT = 10
MAX_OBJECTS_COUNT = 30

MIN_LEVEL = 1
MAX_LEVEL = 100

COUNT_OF_XML_FILES_IN_ZIP_FILE = 100
COUNT_OF_ZIP_FILES = 50

TEMP_DIR_FOR_XML_FILES = '/home/xmlfiles'


def create_xml_file_and_get_filename(dirname=TEMP_DIR_FOR_XML_FILES):
    unique_string = str(uuid.uuid4())
    filename = '{}/{}.xml'.format(dirname, unique_string)

    root = ET.Element('root')
    ET.SubElement(root, 'var', attrib={'name': 'id', 'value': unique_string})
    level_value = str(random.randrange(MIN_LEVEL, MAX_LEVEL + 1))
    ET.SubElement(root, 'var', attrib={'name': 'level', 'value': level_value})

    objects = ET.SubElement(root, 'objects')
    objects_count = random.randrange(MIN_OBJECTS_COUNT, MAX_OBJECTS_COUNT + 1)
    for _ in range(objects_count):
        ET.SubElement(objects, 'object', attrib={'name': get_random_str()})

    tree = ET.ElementTree(root)
    tree.write(filename)

    return filename


def create_zip_file(archname):
    with ZipFile(archname, 'w') as zip_file:
        for _ in range(COUNT_OF_XML_FILES_IN_ZIP_FILE):
            xml_filename = create_xml_file_and_get_filename()
            zip_file.write(xml_filename, arcname=xml_filename.split('/')[-1])


def create_dir_with_zip_files(dirname_for_zip_files):
    create_empty_dir(TEMP_DIR_FOR_XML_FILES)
    create_empty_dir(dirname_for_zip_files)

    for _ in range(COUNT_OF_ZIP_FILES):
        filename = '{}/{}.zip'.format(dirname_for_zip_files, get_random_str())
        create_zip_file(filename)
    remove_files_in_dir(TEMP_DIR_FOR_XML_FILES)
